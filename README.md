# README #

DragableList provides an AngularJS directive that builds up a searchable list whose elements can be dragged into another DraggableList. Dragging an element into another list also moves the element in the other list of your controller.

**[Check out this demo](http://domes-website.de/dev/angular/dragable-list)**


## **Dependencies** ##

* UI.Bootstrap

* Bootstrap css

* AngularJS


## **The directive takes** ##

* a title **title**

* a description from your controller **description**

* a list of your controller **list**

* **width** and **height**

* a function of your controller taking *elem, from, to, fromStr, toStr* as arguments that is called after the dragged element was moved into the other list in your controller **run-after-drop**

as attributes and takes an Angular expression as body.


## **Look at the following example:** ##

```
#!html

<dragable-list list="list1" width="250" height="500"
      description="list1desc" title="List #1"
      run-after-drop="afterDrop(elem, from, to, fromStr, toStr)">
  element.name
</dragable-list>
```

This code creates a DragableList with **List #1** as title, **$scope.list1desc** as description which is 250px wide and 500px high. It displays **$scope.list1**, showing each elements *name* attribute (**element.name**). After dropping an element from another DragableList in this list, the function **$scope.afterDrop** is called with the following arguments:

* *elem* which is the currently dragged element

* *from* which is the list where *elem* comes from

* *to* which is the list where *elem* was dropped into

* *fromStr* and *toStr* which are the names of the lists