(function() {
  'use strict';
  angular.module('domesDragableList', ['ui.bootstrap']).directive('dragableList', function() {
    return {
      templateUrl: 'components/directives/dragableList/dragableList.html',
      restrict: 'E',
      transclude: true,
      scope: {
        list: '=list',
        width: '@width',
        height: '@height',
        title: '@title',
        description: '=description',
        runAfterDrop: '&'
      },
      compile: function(element, attrs, transclude) {
        if (!attrs.width) {
          attrs.width = 300;
        }
        if (!attrs.height) {
          attrs.height = 450;
        } else {
          attrs.height = parseInt(attrs.height);
        }
        if (!attrs.title) {
          attrs.title = attrs.list;
        }
        if (!attrs.list) {
          attrs.title = 'No list specified!';
        }
        element.children()[0].setAttribute('lst', attrs.list);
        return function(scope) {
          transclude(scope, function(clone) {
            scope.transcludedContent = clone[0].textContent;
          });
          scope.dropped = function(dragEl, dropEl) {
            var drag, drop, fromIndex, fromIndexStr, fromList, fromListStr, toList, toListStr;
            drag = angular.element(dragEl);
            drop = angular.element(dropEl);
            fromListStr = dragEl.parentNode.parentNode.attributes.getNamedItem('lst').value;
            fromIndexStr = drag.attr('indexedid');
            toListStr = drop.attr('lst');
            fromIndex = parseInt(fromIndexStr.substring(0, fromIndexStr.indexOf('_')));
            fromList = eval('scope.$parent.' + fromListStr);
            toList = eval('scope.$parent.' + toListStr);
            element = fromList.splice(fromIndex, 1)[0];
            toList.push(element);
            scope.$parent.$apply();
            scope.runAfterDrop({
              elem: element,
              from: fromList,
              to: toList,
              fromStr: fromListStr,
              toStr: toListStr
            });
          };
        };
      }
    };
  }).factory('uuid', function() {
    var svc;
    svc = {
      newuuid: function() {
        var _p8;
        _p8 = function(s) {
          var p;
          p = (Math.random().toString(16) + '000000000').substr(2, 8);
          if (s) {
            return '-' + p.substr(0, 4) + '-' + p.substr(4, 4);
          } else {
            return p;
          }
        };
        return _p8() + _p8(true) + _p8(true) + _p8();
      },
      empty: function() {
        return '00000000-0000-0000-0000-000000000000';
      }
    };
    return svc;
  }).directive('lvlDraggable', [
    '$rootScope', 'uuid', function($rootScope, uuid) {
      return {
        scope: {
          slindex: '@slindex'
        },
        restrict: 'A',
        link: function(scope, el, attrs) {
          var id;
          angular.element(el).attr('draggable', 'true');
          id = angular.element(el).attr('id');
          if (!id) {
            id = uuid.newuuid();
            angular.element(el).attr('id', id);
            angular.element(el).attr('indexedid', attrs.slindex + '_' + id);
          }
          el.bind('dragstart', function(e) {
            e.dataTransfer = e.originalEvent.dataTransfer;
            e.dataTransfer.setData('text', id);
            $rootScope.$emit('LVL-DRAG-START');
          });
          el.bind('dragend', function() {
            $rootScope.$emit('LVL-DRAG-END');
          });
        }
      };
    }
  ]).directive('lvlDropTarget', [
    '$rootScope', 'uuid', function($rootScope, uuid) {
      return {
        restrict: 'A',
        scope: {
          onDrop: '&'
        },
        link: function(scope, el) {
          var id;
          id = angular.element(el).attr('id');
          if (!id) {
            id = uuid.newuuid();
            angular.element(el).attr('id', id);
          }
          el.bind('dragover', function(e) {
            e.dataTransfer = e.originalEvent.dataTransfer;
            if (e.preventDefault) {
              e.preventDefault();
            }
            e.dataTransfer.dropEffect = 'move';
            return false;
          });
          el.bind('dragenter', function(e) {
            angular.element(e.target).addClass('lvl-over');
          });
          el.bind('dragleave', function(e) {
            angular.element(e.target).removeClass('lvl-over');
          });
          el.bind('drop', function(e) {
            var data, dest, src;
            e.dataTransfer = e.originalEvent.dataTransfer;
            if (e.preventDefault) {
              e.preventDefault();
            }
            if (e.stopPropagation) {
              e.stopPropagation();
            }
            data = e.dataTransfer.getData('text');
            dest = document.getElementById(id);
            src = document.getElementById(data);
            scope.onDrop({
              drag: src,
              drop: dest
            });
          });
          $rootScope.$on('LVL-DRAG-START', function() {
            el = document.getElementById(id);
            angular.element(el).addClass('lvl-target');
          });
          $rootScope.$on('LVL-DRAG-END', function() {
            el = document.getElementById(id);
            angular.element(el).removeClass('lvl-target');
            angular.element(el).removeClass('lvl-over');
          });
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=dragableList.js.map
